#nginx load balancer
resource "aws_lb" "GrainChain_LB" {
    name = "GrainChain-LB"
    internal = false
    load_balancer_type = "application"    

    subnets = [
        aws_subnet.public_1.id,
        aws_subnet.public_2.id
    ]

    security_groups = [ aws_security_group.LoadBalancer_security_group.id ]
    tags = {
        Name = "GrainChain LB"
    }

    depends_on = [
        aws_vpc.GrainChain_VPC,
        aws_subnet.public_1,
        aws_subnet.public_2
    ]
}

resource "aws_lb_listener" "GrainChain_Listener" {
    load_balancer_arn = aws_lb.GrainChain_LB.arn
    protocol = "HTTP"
    port = 80

    default_action {
        target_group_arn = aws_lb_target_group.GrainChain_TargetGroup.arn
        type = "forward"
    }
}

resource "aws_lb_target_group" "GrainChain_TargetGroup" {
    name = "GrainChain-TargetGroup"
    port = 80
    protocol = "HTTP"
    vpc_id = aws_vpc.GrainChain_VPC.id

    health_check {
      path = "/"
      interval = 10
      healthy_threshold = 3
      unhealthy_threshold = 3
      timeout = 5
    }

    tags = {
        Name = "GrainChain_TargetGroup"
    }
}


resource "aws_lb_target_group_attachment" "GrainChain_TargetGroup_Attachment_1" {
    target_group_arn = aws_lb_target_group.GrainChain_TargetGroup.arn
    target_id = aws_instance.nginx_private_1.id
    port = 80
}

resource "aws_lb_target_group_attachment" "GrainChain_TargetGroup_Attachment_2" {
    target_group_arn = aws_lb_target_group.GrainChain_TargetGroup.arn
    target_id = aws_instance.nginx_private_2.id
    port = 80
}

output "GrainChain_LB_URL" {
    value = aws_lb.GrainChain_LB.dns_name
    description = "GrainChain LB URL"
}