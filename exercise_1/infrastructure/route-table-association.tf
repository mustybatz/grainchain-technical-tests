resource "aws_route_table_association" "public_1_route_table_association" {
  subnet_id = aws_subnet.public_1.id

  route_table_id = aws_route_table.public_route_table.id
}


resource "aws_route_table_association" "public_2_route_table_association" {
  subnet_id = aws_subnet.public_2.id

  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table_association" "private_1_route_table_association" {
  subnet_id = aws_subnet.private_1.id

  route_table_id = aws_route_table.private_1.id
}


resource "aws_route_table_association" "private_2_route_table_association" {
  subnet_id = aws_subnet.private_2.id

  route_table_id = aws_route_table.private_2.id
}
