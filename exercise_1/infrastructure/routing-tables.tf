resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.GrainChain_VPC.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.GrainChain_VPC_InternetGateway.id
  }

  tags = {
    Name = "GrainChain_Public_Route_Table"
  }
}

resource "aws_route_table" "private_1" {
  vpc_id = aws_vpc.GrainChain_VPC.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.NAT1.id
  }

  tags = {
    Name = "GrainChain_Private_1_Route_Table"
  }
}

resource "aws_route_table" "private_2" {
  vpc_id = aws_vpc.GrainChain_VPC.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.NAT2.id
  }

  tags = {
    Name = "GrainChain_Private_2_Route_Table"
  }
}
