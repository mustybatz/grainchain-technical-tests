resource "aws_eip_association" "bastion_1_eip_association" {
  instance_id   = aws_instance.bastion_1.id
  allocation_id = aws_eip.bastion_1_eip.id
}

resource "aws_eip_association" "bastion_2_eip_association" {
  instance_id   = aws_instance.bastion_2.id
  allocation_id = aws_eip.bastion_2_eip.id
}

output "bastion_1_eip" {
  value       = aws_eip.bastion_1_eip.public_ip
  description = "Bastion 1 EIP Association ID"
}

output "bastion_2_eip" {
  value       = aws_eip.bastion_2_eip.public_ip
  description = "Bastion 2 EIP Association ID"
}
