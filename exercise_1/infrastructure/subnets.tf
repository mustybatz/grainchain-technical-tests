resource "aws_subnet" "public_1" {
  vpc_id = aws_vpc.GrainChain_VPC.id

  cidr_block = "192.168.0.0/18"

  availability_zone = "us-west-2a"

  map_public_ip_on_launch = true

  tags = {
    "Name" = "public-us-west-2a"
  }
}

resource "aws_subnet" "public_2" {
  vpc_id = aws_vpc.GrainChain_VPC.id

  cidr_block = "192.168.64.0/18"

  availability_zone = "us-west-2b"

  map_public_ip_on_launch = true

  tags = {
    "Name" = "public-us-west-2b"
  }
}

resource "aws_subnet" "private_1" {
  vpc_id = aws_vpc.GrainChain_VPC.id

  cidr_block = "192.168.128.0/18"

  availability_zone = "us-west-2a"

  tags = {
    "Name" = "private-us-west-2a"
  }
}

resource "aws_subnet" "private_2" {
  vpc_id = aws_vpc.GrainChain_VPC.id

  cidr_block = "192.168.192.0/18"

  availability_zone = "us-west-2b"

  tags = {
    "Name" = "private-us-west-2b"
  }
}


output "public_1_id" {
  value       = aws_subnet.public_1.id
  description = "Public Subnet 1 ID"
}

output "public_2_id" {
  value       = aws_subnet.public_2.id
  description = "Public Subnet 2 ID"
}

output "private_1_id" {
  value       = aws_subnet.private_1.id
  description = "Private Subnet 1 ID"
}

output "private_2_id" {
  value       = aws_subnet.private_2.id
  description = "Private Subnet 2 ID"
}