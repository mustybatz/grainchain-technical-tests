
resource "aws_internet_gateway" "GrainChain_VPC_InternetGateway" {

  vpc_id = aws_vpc.GrainChain_VPC.id

  tags = {
    "Name" = "GrainChain_VPC_InternetGateway"
  }
}

output "internet_gateway_id" {
  value = aws_internet_gateway.GrainChain_VPC_InternetGateway.id
}
