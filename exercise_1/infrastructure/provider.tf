terraform {
  required_providers {
    tls = {
      source  = "hashicorp/tls"
      version = "3.3.0"
    }
  }
}

provider "tls" {
}

provider "aws" {
  region = "us-west-2"
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "barush-terraform-1"

  lifecycle {
    prevent_destroy = true
  }

  versioning {
    enabled = true
  }
}