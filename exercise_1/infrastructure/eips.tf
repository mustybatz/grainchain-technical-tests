resource "aws_eip" "nat1" {
  depends_on = [
    aws_internet_gateway.GrainChain_VPC_InternetGateway
  ]

  tags = {
    Name = "(NAT 1) EIP"
  }
}

resource "aws_eip" "nat2" {
  depends_on = [
    aws_internet_gateway.GrainChain_VPC_InternetGateway
  ]

  tags = {
    Name = "(NAT 2) EIP"
  }
}

resource "aws_eip" "bastion_1_eip" {
  depends_on = [
    aws_internet_gateway.GrainChain_VPC_InternetGateway
  ]

  tags = {
    "Name" = "Bastion 1 EIP"
  }
}

resource "aws_eip" "bastion_2_eip" {
  depends_on = [
    aws_internet_gateway.GrainChain_VPC_InternetGateway
  ]

  tags = {
    "Name" = "Bastion 2 EIP"
  }
}
