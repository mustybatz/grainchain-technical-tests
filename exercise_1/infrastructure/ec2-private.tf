resource "aws_instance" "nginx_private_1" {
  ami                    = "ami-0ee8244746ec5d6d4"
  instance_type          = "t3.small"
  subnet_id              = aws_subnet.private_1.id
  key_name               = var.private_instance_key
  vpc_security_group_ids = [aws_security_group.private_instance_security_group_1.id]
  availability_zone      = "us-west-2a"

  user_data = <<-EOF
  #!/bin/bash
  apt update
  apt upgrade -y
  apt install -y nginx
  ufw allow 'Nginx HTTP'
  ufw allow 'OpenSSH'
  ufw enable
  service nginx start
  EOF

  tags = {
    Name = "GrainChain_Nginx_Private_1"
  }
}

resource "aws_instance" "nginx_private_2" {
  ami                    = "ami-0ee8244746ec5d6d4"
  instance_type          = "t3.small"
  subnet_id              = aws_subnet.private_2.id
  key_name               = var.private_instance_key
  vpc_security_group_ids = [aws_security_group.private_instance_security_group_2.id]
  availability_zone      = "us-west-2b"

  user_data = <<-EOF
  #!/bin/bash
  apt update
  apt upgrade -y
  apt install -y nginx
  ufw allow 'Nginx HTTP'
  ufw allow 'OpenSSH'
  ufw enable
  service nginx start
  EOF


  tags = {
    Name = "GrainChain_Nginx_Private_2"
  }
}

output "nginx_private_1_ip" {
  value = aws_instance.nginx_private_1.private_ip
}

output "nginx_private_2_ip" {
  value = aws_instance.nginx_private_2.private_ip
}