terraform {
  backend "s3" {
    bucket = "barush-terraform-1"
    key    = "terraform.tfstate"
    region = "us-west-2"
  }
}