# Load balancer security group
resource "aws_security_group" "LoadBalancer_security_group" {
  name   = "LoadBalancer_security_group"
  vpc_id = aws_vpc.GrainChain_VPC.id

  ingress {
    description = "Allow access to the load balancer"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
  }

  egress {
    protocol  = "-1"
    from_port = 0
    to_port   = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  depends_on = [
    aws_vpc.GrainChain_VPC,
    aws_subnet.public_1,
    aws_subnet.public_2
  ]

  tags = {
    Name = "LoadBalancer_security_group"
  }
}


resource "aws_security_group" "private_instance_security_group_1" {
  description = "Nginx bastion host access"
  name        = "private_instance_security_group_1"

  vpc_id = aws_vpc.GrainChain_VPC.id

  ingress {
    description     = "Bastion host sg"
    from_port       = 22
    to_port         = 22
    protocol        = "TCP"
    security_groups = [aws_security_group.Bastion_security_group_1.id]
  }

  ingress {
    description = "80 port access from alb"
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"

    security_groups = [aws_security_group.LoadBalancer_security_group.id]

    cidr_blocks = [
      aws_subnet.public_1.cidr_block,
      aws_subnet.public_2.cidr_block
    ]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  depends_on = [
    aws_vpc.GrainChain_VPC,
    aws_subnet.private_1,
    aws_subnet.private_2
  ]
}

resource "aws_security_group" "private_instance_security_group_2" {
  description = "Nginx bastion host access 2"
  name        = "private_instance_security_group_2"

  vpc_id = aws_vpc.GrainChain_VPC.id

  ingress {
    description     = "Bastion host sg"
    from_port       = 22
    to_port         = 22
    protocol        = "TCP"
    security_groups = [aws_security_group.Bastion_security_group_2.id]
  }

  ingress {
    description = "80 port access from alb"
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"

    security_groups = [aws_security_group.LoadBalancer_security_group.id]

    cidr_blocks = [
      aws_subnet.public_1.cidr_block,
      aws_subnet.public_2.cidr_block
    ]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  depends_on = [
    aws_vpc.GrainChain_VPC,
    aws_subnet.private_1,
    aws_subnet.private_2,
  ]
}

resource "aws_security_group" "Bastion_security_group_1" {
  name   = "Bastion_security_group_1"
  vpc_id = aws_vpc.GrainChain_VPC.id

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow SSH access through port 2422"
    from_port   = 2422
    protocol    = "TCP"
    to_port     = 2422
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  depends_on = [
    aws_vpc.GrainChain_VPC,
    aws_subnet.public_1,
    aws_subnet.public_2
  ]

}

resource "aws_security_group" "Bastion_security_group_2" {
  name   = "Bastion_security_group_2"
  vpc_id = aws_vpc.GrainChain_VPC.id

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow SSH access through port 2422"
    from_port   = 2422
    protocol    = "TCP"
    to_port     = 2422
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  depends_on = [
    aws_vpc.GrainChain_VPC,
    aws_subnet.public_1,
    aws_subnet.public_2
  ]

}

