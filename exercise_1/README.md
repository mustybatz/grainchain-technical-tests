# Exercise

In this exercise, I needed to create the following architecture using Terraform and Ansible:

![](images/architecture.png)

In which I had to create multiple resources like:
- One VPC where our architecture will live
- Two different availability zones
- Two public subnets (on different availability zones)
- Two private subnets (on different availability zones)
- Two NAT Gateways on each public subnet
- Two bastion hosts on each public subnet
- Two NGINX instances on each private subnet
- One application load balancer as NGINX primary endpoint
- An Internet Gateway

But before we start with our infrastructure, let's configure a remote backend using gitlab.

## Remote Backend
---
I want to store all the terraform state somewhere safe, so first I'll configure a gitlab remote backend and use it for uploading our state. This also allows other people to work on the same infrastructure at the same time.


Inside our `.gitlab-ci.yml` file, lets add some commands for our CI/CD workflow:

``` Yaml
include:
  - template: Terraform/Base.gitlab-ci.yml  

stages:
  - init
  - validate
  - build
  - deploy

variables:
  TF_ROOT: "./exercise_1/infrastructure"

init:
  extends: .init
  rules:
    - changes:
        - "exercise_1/infrastructure/**"

validate:
  extends: .validate
  rules:
    - changes:
        - "exercise_1/infrastructure/**"

build:
  extends: .build
  rules:
    - changes:
        - "exercise_1/infrastructure/**"

deploy:
  extends: .deploy
  dependencies:
    - build
```

Here we provided an existing gitlab template that allows us to configure terraform CI/CD, Terraform pipelines will only run if changes are made into the `exercise_1/terraform` directory. 

Now that we have CI/CD configured and our terraform state is safe on a remote backend, let's build our infrastructure.

## Creating our VPC
---

First, we need to create a VPC for our architecture, so let's create a `provider.tf` file and a `vpc.tf` file:
 
`provider.tf` \
Here we need to define that we'll be using AWS as our cloud provider and the default region, in this case we are using `us-west-2`.
``` Terraform
provider "aws" {
  region = "us-west-2"
}
```

`vpc.tf` \
On this file we declare our VPC and name it as GrainChain_VPC, it uses the `192.168.0.0/16` CIDR block and has dns support and dns hostnames enabled.
``` Terraform
resource "aws_vpc" "GrainChain_VPC" {
  cidr_block = "192.168.0.0/16"

  instance_tenancy = "default"

  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "GrainChain_VPC"
  }
}

output "vpc_id" {
  value       = aws_vpc.GrainChain_VPC.id
  description = "VPC ID"
}

```

## Internet Gateway
---
In order to connect our VPC to the internet, we need to create an Internet Gateway, so let's create `internet_gateway.tf` file and configure it:

``` Terraform

resource "aws_internet_gateway" "GrainChain_VPC_InternetGateway" {
  vpc_id = aws_vpc.GrainChain_VPC.id

  tags = {
    "Name" = "GrainChain_VPC_InternetGateway"
  }
}

output "internet_gateway_id" {
  value = aws_internet_gateway.GrainChain_VPC_InternetGateway.id
}
```

## Subnets
---
Now we can create our subnets, we need two public and private subnets in different availability zones, so let's create `subnets.tf` and configure them:

``` Terraform
resource "aws_subnet" "public_1" {
  vpc_id = aws_vpc.GrainChain_VPC.id

  cidr_block = "192.168.0.0/18"

  availability_zone = "us-west-2a"

  map_public_ip_on_launch = true

  tags = {
    "Name" = "public-us-west-2a"
  }
}

resource "aws_subnet" "public_2" {
  vpc_id = aws_vpc.GrainChain_VPC.id

  cidr_block = "192.168.64.0/18"

  availability_zone = "us-west-2b"

  map_public_ip_on_launch = true

  tags = {
    "Name" = "public-us-west-2b"
  }
}

resource "aws_subnet" "private_1" {
  vpc_id = aws_vpc.GrainChain_VPC.id

  cidr_block = "192.168.128.0/18"

  availability_zone = "us-west-2a"

  tags = {
    "Name" = "private-us-west-2a"
  }
}

resource "aws_subnet" "private_2" {
  vpc_id = aws_vpc.GrainChain_VPC.id

  cidr_block = "192.168.192.0/18"

  availability_zone = "us-west-2b"

  tags = {
    "Name" = "private-us-west-2b"
  }
}


output "public_1_id" {
  value       = aws_subnet.public_1.id
  description = "Public Subnet 1 ID"
}

output "public_2_id" {
  value       = aws_subnet.public_2.id
  description = "Public Subnet 2 ID"
}

output "private_1_id" {
  value       = aws_subnet.private_1.id
  description = "Private Subnet 1 ID"
}

output "private_2_id" {
  value       = aws_subnet.private_2.id
  description = "Private Subnet 2 ID"
}
```

At this point our subnets are not public nor private, we need to create route tables and setup two nat gateways so they can be private an public.

## Nat Gateways
---
Our nginx servers need internet access so they can download updates and nginx itself. To achieve that, we need to setup two NAT gateways inside our public subnet, every time that our servers requests internet access, they will route to its respective NAT gateway. Additionally, NAT gateways need to have an elastic IP address, so let's create `eips.tf` and `nat-gateways.tf`:

`eips.tf` \
Here we create two elastic IPs and name them `nat1` and `nat2` respectively
``` Terraform
resource "aws_eip" "nat1" {
  depends_on = [
    aws_internet_gateway.GrainChain_VPC_InternetGateway
  ]

  tags = {
    Name = "(NAT 1) EIP"
  }
}

resource "aws_eip" "nat2" {
  depends_on = [
    aws_internet_gateway.GrainChain_VPC_InternetGateway
  ]

  tags = {
    Name = "(NAT 2) EIP"
  }
```
`nat-gateways.tf` \
Now we create two NAT gateways and bind each elastic IP into them
``` Terraform
resource "aws_nat_gateway" "NAT1" {
  allocation_id = aws_eip.nat1.id
  subnet_id     = aws_subnet.public_1.id
  

  tags = {
    Name = "NAT 1"
  }
}


resource "aws_nat_gateway" "NAT2" {
  allocation_id = aws_eip.nat2.id
  subnet_id     = aws_subnet.public_2.id

  tags = {
    Name = "NAT 2"
  }
}
```
Cool! Now our private subnets can reach the internet, we just need to setup route tables.


## Routing tables
---
We'll be creating three routing tables, one for our public subnets and two for each private subnet. We need to create `routing-tables.tf` to configure the route tables and `route-table-association.tf` so we can bind them with each subnet:

`routing-tables.tf` \
Our public subnets are routing all the traffic that is directed to any ip different than local CIDR using the `0.0.0.0/0` wildcard, all the requests that matches the wildcard are being routed into our Internet Gateway. On the other hand, every time that our private instances try to reach any ip different than the local network, the traffic will be redirected to NAT1 or NAT2.

``` Terraform
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.GrainChain_VPC.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.GrainChain_VPC_InternetGateway.id
  }

  tags = {
    Name = "GrainChain_Public_Route_Table"
  }
}

resource "aws_route_table" "private_1" {
  vpc_id = aws_vpc.GrainChain_VPC.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.NAT1.id
  }

  tags = {
    Name = "GrainChain_Private_1_Route_Table"
  }
}

resource "aws_route_table" "private_2" {
  vpc_id = aws_vpc.GrainChain_VPC.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.NAT2.id
  }

  tags = {
    Name = "GrainChain_Private_2_Route_Table"
  }
}
```

Now, we need to associate the routing tables with our subnets and we do that on the `route-table-association.tf` file:

``` Terraform
resource "aws_route_table_association" "public_1_route_table_association" {
  subnet_id = aws_subnet.public_1.id

  route_table_id = aws_route_table.public_route_table.id
}


resource "aws_route_table_association" "public_2_route_table_association" {
  subnet_id = aws_subnet.public_2.id

  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table_association" "private_1_route_table_association" {
  subnet_id = aws_subnet.private_1.id

  route_table_id = aws_route_table.private_1.id
}


resource "aws_route_table_association" "private_2_route_table_association" {
  subnet_id = aws_subnet.private_2.id

  route_table_id = aws_route_table.private_2.id
}
```
As we can see, both of our public subnets are associated with our public route table, private subnet #1 is associated with `private_1` route table and private subnet #2 is associated with 
`private_2` route table. Therefore our private subnet 1 is connected with NAT 1 and private subnet 2 is connected with NAT 2. 

## Bastion hosts
--- 
In order to connect to the nginx instances, we need a bastion host. We can't connect directly into our private instances because they don't have ingress internet access, therefore we need to setup a bastion host on each public subnet and allow SSH traffic through port `2422` and assign an elastic IP to them. So let's create `ec2_public.tf` and `security-groups.tf` to configure our bastion hosts.

Our bastion hosts need security groups so we can specify ingress and egress traffic, we need two separate security groups because our nginx_1 instance can only connect into private_1 instance and our nginx_2 instance can only connect into private_2 instance, we don't have cross connection enabled.

`security-groups.tf`: 
``` Terraform
resource "aws_security_group" "Bastion_security_group_1" {
  name   = "Bastion_security_group_1"
  vpc_id = aws_vpc.GrainChain_VPC.id

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow SSH access through port 2422"
    from_port   = 2422
    protocol    = "TCP"
    to_port     = 2422
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  depends_on = [
    aws_vpc.GrainChain_VPC,
    aws_subnet.public_1,
    aws_subnet.public_2
  ]

}

resource "aws_security_group" "Bastion_security_group_2" {
  name   = "Bastion_security_group_2"
  vpc_id = aws_vpc.GrainChain_VPC.id

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow SSH access through port 2422"
    from_port   = 2422
    protocol    = "TCP"
    to_port     = 2422
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  depends_on = [
    aws_vpc.GrainChain_VPC,
    aws_subnet.public_1,
    aws_subnet.public_2
  ]

}
```

In order to connect into our bastion hosts we need to setup a key pair, so let's create another file named `key-pairs.tf` and create them:

``` Terraform
variable "bastion_key" {
  type    = string
  default = "GrainChain_Key"
}

resource "tls_private_key" "public_instance_key_tls" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = var.bastion_key
  public_key = tls_private_key.public_instance_key_tls.public_key_openssh
}

output "ssh_key" {
  description = "ssh key generated by terraform"
  value       = tls_private_key.public_instance_key_tls.private_key_pem
  sensitive   = true
}
```

Now we can create our bastion hosts and place them different availability zones \
`ec2_public.tf`:


``` Terraform
resource "aws_instance" "bastion_1" {
  ami                         = "ami-0ee8244746ec5d6d4"
  instance_type               = "t3.small"
  subnet_id                   = aws_subnet.public_1.id
  key_name                    = var.bastion_key
  vpc_security_group_ids      = [aws_security_group.Bastion_security_group_1.id]
  associate_public_ip_address = true
  availability_zone = "us-west-2a"


  user_data = <<-EOF
    #!/bin/bash
    echo "Port 2422" >> /etc/ssh/sshd_config
    systemctl restart sshd
    EOF

  tags = {
    Name = "GrainChain_Bastion_1"
  }
}

resource "aws_instance" "bastion_2" {
  ami                         = "ami-0ee8244746ec5d6d4"
  instance_type               = "t3.small"
  subnet_id                   = aws_subnet.public_2.id
  key_name                    = var.bastion_key
  vpc_security_group_ids      = [aws_security_group.Bastion_security_group_2.id]
  associate_public_ip_address = true
  availability_zone = "us-west-2b"

  user_data = <<-EOF
    #!/bin/bash
    echo "Port 2422" >> /etc/ssh/sshd_config
    systemctl restart sshd
    EOF

  tags = {
    Name = "GrainChain_Bastion_2"
  }
}


```

Note that we added user data, this is so we can change the default ssh port on the ubuntu sshd config at instance creation.

## Nginx instances
---
Now we can configure our Nginx instances, we need to setup a security group for each instance on the `security-groups.tf` file, add a new ssh key on the `key-pairs.tf` file, create a new file named `ec2-private.tf` and add two ec2 instances.

Let's begin with our security groups: \

`security-groups.tf`
```Terraform
  resource "aws_security_group" "private_instance_security_group_1" {
  description = "Nginx bastion host access"
  name        = "private_instance_security_group_1"

  vpc_id = aws_vpc.GrainChain_VPC.id

  ingress {
    description     = "Bastion host sg"
    from_port       = 22
    to_port         = 22
    protocol        = "TCP"
    security_groups = [aws_security_group.Bastion_security_group_1.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  depends_on = [
    aws_vpc.GrainChain_VPC,
    aws_subnet.private_1,
    aws_subnet.private_2
  ]
}

resource "aws_security_group" "private_instance_security_group_2" {
  description = "Nginx bastion host access 2"
  name        = "private_instance_security_group_2"

  vpc_id = aws_vpc.GrainChain_VPC.id

  ingress {
    description     = "Bastion host sg"
    from_port       = 22
    to_port         = 22
    protocol        = "TCP"
    security_groups = [aws_security_group.Bastion_security_group_2.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  depends_on = [
    aws_vpc.GrainChain_VPC,
    aws_subnet.private_1,
    aws_subnet.private_2
  ]
}
```

With this configuration now we have allowed ssh traffic on port 22 only for each bastion host. Now let's create an ssh key to access to our server through bastion hosts:

`key-pairs.tf`
``` Terraform
variable "private_instance_key" {
  type    = string
  default = "GrainChain_private_key"
}

resource "tls_private_key" "private_instance_key_tls" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "private_instance_key" {
  key_name   = var.private_instance_key
  public_key = tls_private_key.private_instance_key_tls.public_key_openssh
}

output "ssh_key_private" {
  description = "Private instance ssh key generated by terraform"
  value       = tls_private_key.private_instance_key_tls.private_key_pem
  sensitive   = true
}
```

Now that we have created our ssh keys, we can go ahead and create our ec2 instances and install nginx on them:

`ec2-private.tf`
```
resource "aws_instance" "nginx_private_1" {
  ami                    = "ami-0ee8244746ec5d6d4"
  instance_type          = "t3.small"
  subnet_id              = aws_subnet.private_1.id
  key_name               = var.private_instance_key
  vpc_security_group_ids = [aws_security_group.private_instance_security_group_1.id]
  availability_zone      = "us-west-2a"

  user_data = <<-EOF
  #!/bin/bash
  apt update
  apt upgrade -y
  apt install -y nginx
  ufw allow 'Nginx HTTP'
  ufw allow 'OpenSSH'
  ufw enable
  service nginx start
  EOF

  tags = {
    Name = "GrainChain_Nginx_Private_1"
  }
}

resource "aws_instance" "nginx_private_2" {
  ami                    = "ami-0ee8244746ec5d6d4"
  instance_type          = "t3.small"
  subnet_id              = aws_subnet.private_2.id
  key_name               = var.private_instance_key
  vpc_security_group_ids = [aws_security_group.private_instance_security_group_2.id]
  availability_zone      = "us-west-2b"

  user_data = <<-EOF
  #!/bin/bash
  apt update
  apt upgrade -y
  apt install -y nginx
  ufw allow 'Nginx HTTP'
  ufw allow 'OpenSSH'
  ufw enable
  service nginx start
  EOF


  tags = {
    Name = "GrainChain_Nginx_Private_2"
  }
}

output "nginx_private_1_ip" {
  value = aws_instance.nginx_private_1.private_ip
}

output "nginx_private_2_ip" {
  value = aws_instance.nginx_private_2.private_ip
}
```
Now that we have nginx installed on our private servers, we can configure our load balancer and connect through a browser.

## Load balancer
---
As a requirement, we need to go through an application load balancer to access the nginx website, let's create a security group to allow traffic through port 80.

`security-groups.tf`
``` Terraform
resource "aws_security_group" "LoadBalancer_security_group" {
  name   = "LoadBalancer_security_group"
  vpc_id = aws_vpc.GrainChain_VPC.id

  ingress {
    description = "Allow access to the load balancer"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
  }

  egress {
    protocol  = "-1"
    from_port = 0
    to_port   = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  depends_on = [
    aws_vpc.GrainChain_VPC,
    aws_subnet.public_1,
    aws_subnet.public_2
  ]

  tags = {
    Name = "LoadBalancer_security_group"
  }
}
```
We also need to allow traffic from load balancer on the nginx instances security groups:

```Terraform

resource "aws_security_group" "private_instance_security_group_1" {
  description = "Nginx bastion host access"
  name        = "private_instance_security_group_1"

  vpc_id = aws_vpc.GrainChain_VPC.id

  ingress {
    description     = "Bastion host sg"
    from_port       = 22
    to_port         = 22
    protocol        = "TCP"
    security_groups = [aws_security_group.Bastion_security_group_1.id]
  }

  ingress {
    description = "80 port access from alb"
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"

    security_groups = [aws_security_group.LoadBalancer_security_group.id]

    cidr_blocks = [
      aws_subnet.public_1.cidr_block,
      aws_subnet.public_2.cidr_block
    ]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  depends_on = [
    aws_vpc.GrainChain_VPC,
    aws_subnet.private_1,
    aws_subnet.private_2
  ]
}

resource "aws_security_group" "private_instance_security_group_2" {
  description = "Nginx bastion host access 2"
  name        = "private_instance_security_group_2"

  vpc_id = aws_vpc.GrainChain_VPC.id

  ingress {
    description     = "Bastion host sg"
    from_port       = 22
    to_port         = 22
    protocol        = "TCP"
    security_groups = [aws_security_group.Bastion_security_group_2.id]
  }

  ingress {
    description = "80 port access from alb"
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"

    security_groups = [aws_security_group.LoadBalancer_security_group.id]

    cidr_blocks = [
      aws_subnet.public_1.cidr_block,
      aws_subnet.public_2.cidr_block
    ]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  depends_on = [
    aws_vpc.GrainChain_VPC,
    aws_subnet.private_1,
    aws_subnet.private_2,
  ]
}
```

Now we can create our load balancer in a new file called `lb.tf`:

```Terraform
#nginx load balancer
resource "aws_lb" "GrainChain_LB" {
    name = "GrainChain-LB"
    internal = false
    load_balancer_type = "application"    

    subnets = [
        aws_subnet.public_1.id,
        aws_subnet.public_2.id
    ]

    security_groups = [ aws_security_group.LoadBalancer_security_group.id ]
    tags = {
        Name = "GrainChain LB"
    }

    depends_on = [
        aws_vpc.GrainChain_VPC,
        aws_subnet.public_1,
        aws_subnet.public_2
    ]
}

resource "aws_lb_listener" "GrainChain_Listener" {
    load_balancer_arn = aws_lb.GrainChain_LB.arn
    protocol = "HTTP"
    port = 80

    default_action {
        target_group_arn = aws_lb_target_group.GrainChain_TargetGroup.arn
        type = "forward"
    }
}

resource "aws_lb_target_group" "GrainChain_TargetGroup" {
    name = "GrainChain-TargetGroup"
    port = 80
    protocol = "HTTP"
    vpc_id = aws_vpc.GrainChain_VPC.id

    health_check {
      path = "/"
      interval = 10
      healthy_threshold = 3
      unhealthy_threshold = 3
      timeout = 5
    }

    tags = {
        Name = "GrainChain_TargetGroup"
    }
}


resource "aws_lb_target_group_attachment" "GrainChain_TargetGroup_Attachment_1" {
    target_group_arn = aws_lb_target_group.GrainChain_TargetGroup.arn
    target_id = aws_instance.nginx_private_1.id
    port = 80
}

resource "aws_lb_target_group_attachment" "GrainChain_TargetGroup_Attachment_2" {
    target_group_arn = aws_lb_target_group.GrainChain_TargetGroup.arn
    target_id = aws_instance.nginx_private_2.id
    port = 80
}

output "GrainChain_LB_URL" {
    value = aws_lb.GrainChain_LB.dns_name
}
```

And that's it, now if we commit our changes into the gitlab repository the infrastructure will be validated, built and deployed. As we push our code, the gitlab CI/CD jobs will execute and give us the load balancer URL:
![](images/cd-terraform.png)

Now we can go to the [load balancer url](http://GrainChain-LB-346871393.us-west-2.elb.amazonaws.com) and verify our configuration:
![](images/nginx-lb.png)

Note: For connecting to the bastion hosts, you need to extract the SSH keys from the remote backend and use ssh agent to manage them for you.