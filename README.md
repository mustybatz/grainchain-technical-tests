# GrainChain-technical-tests

---

This repo is meant to save all artifacts generated by completing the main two technical tests provided by GrainChain. 

Exercices documentation:
 - [Exercice 1](/exercise_1/README.md)
 - [Exercise 2](/exercise_2/Readme.md)