# Excercise #2:

In this exercise, I needed to setup a EKS cluster with 2 nodes of type t3.medium, install a nginx-controller using HELM v3, setup a basic nginx application which displays "Hello Grainchain" which should be accesible through a public address or IP using the nginx controller. I was also instructed to automate builds and image push of the nginx app to a docker hub repo using gitlab CI/CD pipelines and to enable monitoring in EKS cluster using prometheus and grafana.

Let's configure terraform so we can provision an EKS cluster, inside our root directory create a directory named `infrastructure` and the following files:

```
- infrastructure/
    - provider.tf
    - vpc.tf
    - igw.tf
    - subnets.tf
    - nat.tf
    - routes.tf
    - eks.tf
    - nodes.tf
```

Inside `provider.tf` we will define our aws provider and the default version:

```
provider "aws" {
  region = "us-west-2"
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}
```

That will download the aws provider when we run `terraform init`, now let's define our vpc inside `vpc.tf`:

```
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "main"
  }
}
```

Now let's create an internet gateway and attach it to our vpc inside `igw.tf`:

```
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "igw"
  }
}
```

Cool! Now we can create our subnets and place them inside our vpc on the `subnets.tf`:

```
resource "aws_subnet" "private-us-west-2a" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.0.0.0/19"
  availability_zone = "us-west-2a"

  tags = {
    "Name"                            = "private-us-west-2a"
    "kubernetes.io/role/internal-elb" = "1"
    "kubernetes.io/cluster/granchain-test"      = "owned"
  }
}

resource "aws_subnet" "private-us-west-2b" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.0.32.0/19"
  availability_zone = "us-west-2b"

  tags = {
    "Name"                            = "private-us-west-2b"
    "kubernetes.io/role/internal-elb" = "1"
    "kubernetes.io/cluster/granchain-test"      = "owned"
  }
}

resource "aws_subnet" "public-us-west-2a" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.64.0/19"
  availability_zone       = "us-west-2a"
  map_public_ip_on_launch = true

  tags = {
    "Name"                       = "public-us-west-2a"
    "kubernetes.io/role/elb"     = "1"
    "kubernetes.io/cluster/granchain-test" = "owned"
  }
}

resource "aws_subnet" "public-us-west-2b" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.96.0/19"
  availability_zone       = "us-west-2b"
  map_public_ip_on_launch = true

  tags = {
    "Name"                       = "public-us-west-2b"
    "kubernetes.io/role/elb"     = "1"
    "kubernetes.io/cluster/granchain-test" = "owned"
  }
}
```

At this moment our subnets are private, in order to make them public and private we need to setup route tables `routes.tf`:

```
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main.id

  route = [
    {
      cidr_block                 = "0.0.0.0/0"
      nat_gateway_id             = aws_nat_gateway.nat.id
      carrier_gateway_id         = ""
      destination_prefix_list_id = ""
      egress_only_gateway_id     = ""
      gateway_id                 = ""
      instance_id                = ""
      ipv6_cidr_block            = ""
      local_gateway_id           = ""
      network_interface_id       = ""
      transit_gateway_id         = ""
      vpc_endpoint_id            = ""
      vpc_peering_connection_id  = ""
    },
  ]

  tags = {
    Name = "private"
  }
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  route = [
    {
      cidr_block                 = "0.0.0.0/0"
      gateway_id                 = aws_internet_gateway.igw.id
      nat_gateway_id             = ""
      carrier_gateway_id         = ""
      destination_prefix_list_id = ""
      egress_only_gateway_id     = ""
      instance_id                = ""
      ipv6_cidr_block            = ""
      local_gateway_id           = ""
      network_interface_id       = ""
      transit_gateway_id         = ""
      vpc_endpoint_id            = ""
      vpc_peering_connection_id  = ""
    },
  ]

  tags = {
    Name = "public"
  }
}

resource "aws_route_table_association" "private-us-west-2a" {
  subnet_id      = aws_subnet.private-us-west-2a.id
  route_table_id = aws_route_table.private.id
}

resource "aws_route_table_association" "private-us-west-2b" {
  subnet_id      = aws_subnet.private-us-west-2b.id
  route_table_id = aws_route_table.private.id
}

resource "aws_route_table_association" "public-us-west-2a" {
  subnet_id      = aws_subnet.public-us-west-2a.id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table_association" "public-us-west-2b" {
  subnet_id      = aws_subnet.public-us-west-2b.id
  route_table_id = aws_route_table.public.id
}
```

And now let's create our cluster inside `eks.tf`:

```
resource "aws_iam_role" "granchain-test" {
  name = "eks-cluster-granchain-test"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "granchain-test-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.granchain-test.name
}

resource "aws_eks_cluster" "granchain-test" {
  name     = "granchain-test"
  role_arn = aws_iam_role.granchain-test.arn

  vpc_config {
    subnet_ids = [
      aws_subnet.private-us-west-2a.id,
      aws_subnet.private-us-west-2b.id,
      aws_subnet.public-us-west-2a.id,
      aws_subnet.public-us-west-2b.id
    ]
  }

  depends_on = [aws_iam_role_policy_attachment.granchain-test-AmazonEKSClusterPolicy]
}
```

We just need to configure the cluster nodes, let's do that inside `nodes.tf`:

```
resource "aws_iam_role" "nodes" {
  name = "eks-node-group-nodes"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

resource "aws_iam_role_policy_attachment" "nodes-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.nodes.name
}

resource "aws_iam_role_policy_attachment" "nodes-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.nodes.name
}

resource "aws_iam_role_policy_attachment" "nodes-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.nodes.name
}

resource "aws_eks_node_group" "private-nodes" {
  cluster_name    = aws_eks_cluster.granchain-test.name
  node_group_name = "private-nodes"
  node_role_arn   = aws_iam_role.nodes.arn

  subnet_ids = [
    aws_subnet.private-us-west-2a.id,
    aws_subnet.private-us-west-2b.id
  ]

  capacity_type  = "ON_DEMAND"
  instance_types = ["t3.medium"]

  scaling_config {
    desired_size = 2
    max_size     = 2
    min_size     = 0
  }

  update_config {
    max_unavailable = 1
  }

  labels = {
    role = "general"
  }

  depends_on = [
    aws_iam_role_policy_attachment.nodes-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.nodes-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.nodes-AmazonEC2ContainerRegistryReadOnly,
  ]
}
```

Now let's create our infrastructure:

```bash
  terraform init
  terraform plan
  terraform apply
```

This will create all of our infrastructure but it takes approximately 20 min to provision it, once we finish using it we can destroy it with this command:

```
  terraform destroy
```

Now let's update our kubectl config with aws:
```
 aws eks --region us-west-2 update-kubeconfig --name granchain-test
```

Now we can connect into our cluster and install nginx-controller using helm v3:
```
helm upgrade ingress-nginx ingress-nginx \
--repo https://kubernetes.github.io/ingress-nginx \
--namespace ingress-nginx \
--set controller.metrics.enabled=true \
--set-string controller.podAnnotations."prometheus\.io/scrape"="true" \
--set-string controller.podAnnotations."prometheus\.io/port"="10254" \
--install
```

A few pods should start in the `ingress-nginx` namespace:
```
$ kubectl get pods --namespace=ingress-nginx

NAME                                        READY   STATUS    RESTARTS   AGE
ingress-nginx-controller-5849c9f946-lnrhz   1/1     Running   0          6m9s
```

We can verify connectivity by going to the cluster public IP:
```
$ kubectl get service ingress-nginx-controller --namespace=ingress-nginx

NAME                       TYPE           CLUSTER-IP       EXTERNAL-IP                                                               PORT(S)                      AGE
ingress-nginx-controller   LoadBalancer   172.20.104.155   a3c67417ddb4c4545b4eeb77dfda117f-1042394188.us-west-2.elb.amazonaws.com   80:30233/TCP,443:31482/TCP   7m52s
```

If we connect into our external IP, we should see an nginx 404 error:
![](images/404.png)

Let's validate that our controller is configured for metrics:
```
$ helm get values ingress-nginx --namespace ingress-nginx

USER-SUPPLIED VALUES:
controller:
  metrics:
    enabled: true
  podAnnotations:
    prometheus.io/port: "10254"
    prometheus.io/scrape: "true"
```
The output confirms that our controller is configured for metrics, let's configure prometheus server.

## Deploy and configure prometheus server

The Prometheus server must be configured so that it can discover endpoints of services. If a Prometheus server is already running in the cluster and if it is configured in a way that it can find the ingress controller pods, no extra configuration is needed.

Running the following command deploys prometheus in Kubernetes:
```
kubectl apply --kustomize github.com/kubernetes/ingress-nginx/deploy/prometheus/
```

In order to connect to the prometheus dashboard, we need to obtain the service name and port to forward it:
```
$  kubectl get services --namespace ingress-nginx

ingress-nginx-controller             LoadBalancer   172.20.197.45    a02cbf0b85ece4dd0a60508a5a9ea28a-772032779.us-west-2.elb.amazonaws.com   80:31004/TCP,443:31897/TCP   12m
ingress-nginx-controller-admission   ClusterIP      172.20.129.7     <none>                                                                   443/TCP                      12m
ingress-nginx-controller-metrics     ClusterIP      172.20.214.177   <none>                                                                   10254/TCP                    12m
prometheus-server                    NodePort       172.20.72.85     <none>                                                                   9090:30780/TCP               4m11s
```

The name of our service is `prometheus-server` and the current port is `9090`, now let's forward it:
```
$ kubectl port-forward service/prometheus-server 8080:9090 -n ingress-nginx

Forwarding from 127.0.0.1:8080 -> 9090
Forwarding from [::1]:8080 -> 9090
```

This will allow us to access the prometheus panel through [localhost:8080](http://localhost:8080):
![](images/prom.png)

Now let's add graphana, install it with the following command:
```
$ kubectl apply --kustomize github.com/kubernetes/ingress-nginx/deploy/grafana/

service/grafana created
deployment.apps/grafana created
```

Look at the services:
```
$ kubectl get svc -n ingress-nginx

NAME                                 TYPE           CLUSTER-IP       EXTERNAL-IP                                                              PORT(S)                      AGE
grafana                              NodePort       172.20.67.56     <none>                                                                   3000:31300/TCP               25s
ingress-nginx-controller             LoadBalancer   172.20.197.45    a02cbf0b85ece4dd0a60508a5a9ea28a-772032779.us-west-2.elb.amazonaws.com   80:31004/TCP,443:31897/TCP   28m
ingress-nginx-controller-admission   ClusterIP      172.20.129.7     <none>                                                                   443/TCP                      28m
ingress-nginx-controller-metrics     ClusterIP      172.20.214.177   <none>                                                                   10254/TCP                    28m
prometheus-server                    NodePort       172.20.72.85     <none>                                                                   9090:30780/TCP               20m
```

And forward the port:
```
$ kubectl port-forward service/grafana 8080:9090 -n ingress-nginx

Forwarding from 127.0.0.1:8081 -> 3000
Forwarding from [::1]:8081 -> 3000
```

Now we can access to our panel through [localhost:8081](http://localhost:8081), use `admin` as user and password so you can access the panel:
![](images/GRAFANA.png)

We just need to setup a new dashboard, follow these steps:

  - Hover on the gearwheel icon for configuration and click "Data sources"
  - Select Prometheus
  - Enter the details, use 172.20.72.85:9090 as url
  - Go to "Dashboard" under left menu
  - Click "import"
  - Copy and paste [this](https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/grafana/dashboards/nginx.json) dashboard
  - Click "Import"

Our dashboard should look like this: \
![](/images/dashboard.png)

Now the cluster is configured and we just need to create our nginx application, dockerize it and create a helm chart with it!

# Nginx application

## Folder structure

Inside this directory, we can find another directory called `nginx` which contains two files:

- Dockerfile
- index.html

The Dockerfile only replaces the default nginx `index.html` file with our custom html that displays "Hello grainchain".

```dockerfile
FROM nginx
COPY ./index.html /usr/share/nginx/html/index.html
```

We can build the image:

```bash
docker build -t grainchain-nginx:latest .
```

An then run it as a container:

```bash
docker run --name nginx -p 8080:80 -d  grainchain-nginx:latest
```

Then, if we go to our browser at http://localhost:8080 we can see the following:

![](images/grainchain-index.png)

Cool, the docker image was built correctly and we also run it as a container as it was expected. But we also need to configure CI/CD pipelines for building the image on Gitlab and push them into a public repo on dockerhub.

## Gitlab CI/CD

First, we need to setup some variables so we can authenticate with docker hub without exposing our credentials on the `.gitlab-ci.yml` file, so we need to go to our repo and go to `Settings -> CI/CD -> Variables` and configure the following:

- CI_REGISTRY: we are using docker.io as the value of CI_REGISTRY, but it can be changed to any container registry that you may want to use.
- CI_REGISTRY_USER: Username used on the registry
- CI_REGISTRY_PASSWORD: Password for the user to authenticate to the registry
- CI_REGISTRY_IMAGE: Name of the image stored on the registry, we are using `mustybatz/grainchain-nginx` in this case.

Once we have configured our variables correctly, we can start writing some pipelines on the `.gitlab-ci.yml` file:

```yml
stages:
  - build
  - deploy

docker-build-and-push:
  image: docker:latest
  stage: build

  services:
    - docker:dind

  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY

  script:
    - cd ./excercise_2/nginx
    - docker build --pull -t "$CI_REGISTRY_IMAGE:latest" .
    - docker push "$CI_REGISTRY_IMAGE"

  rules:
    - changes:
        - "excercise_2/nginx/**"
```

The `docker-build-and-push` pipeline does what it's name suggests, it builds the docker image and pushes it into `mustybatz/grainchain-nginx` but the pipeline only runs when changes are made into the `excercise_2/nginx` directory. Let's try it out:

![](images/docker-pipeline.png)

Cool! Now we have an available image at `mustybatz/grainchain-nginx` in dockerhub, let's try to download and run the image on our machine:

First, lets pull the image:

```bash
docker pull mustybatz/grainchain-nginx
```

And now we can run it

```bash
docker run --name nginx -p 8080:80 -d  mustybatz/grainchain-nginx
```

Now we are able to go to our browser to http://localhost:8080 and see our custom html file:
![](images/grainchain-dockerhub.png)

And there you have it, our application is ready to be deployed through containers.

# Helm chart

For creating a new chart we just need to execute the following command:

```bash
helm create nginx-chart
```

This command will create a `nginx-chart` directory with some basic configuration, now we need to modify the `values.yaml` file:

```yaml
# Default values for nginx-grainchain.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

replicaCount: 1

image:
  repository: mustybatz/grainchain-nginx
  pullPolicy: IfNotPresent
  # Overrides the image tag whose default is the chart appVersion.
  tag: "latest"

imagePullSecrets: []
nameOverride: ""
fullnameOverride: ""

serviceAccount:
  # Specifies whether a service account should be created
  create: true
  # Annotations to add to the service account
  annotations: {}
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  name: ""

podAnnotations: {}

podSecurityContext:
  {}
  # fsGroup: 2000

securityContext:
  {}
  # capabilities:
  #   drop:
  #   - ALL
  # readOnlyRootFilesystem: true
  # runAsNonRoot: true
  # runAsUser: 1000

service:
  type: ClusterIP
  port: 80

ingress:
  enabled: false
  className: ""
  annotations:
    {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    - host: chart-example.local
      paths:
        - path: /
          pathType: ImplementationSpecific
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

resources:
  {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #   cpu: 100m
  #   memory: 128Mi
  # requests:
  #   cpu: 100m
  #   memory: 128Mi

autoscaling:
  enabled: false
  minReplicas: 1
  maxReplicas: 100
  targetCPUUtilizationPercentage: 80
  # targetMemoryUtilizationPercentage: 80

nodeSelector: {}

tolerations: []

affinity: {}
```

We added our image repo using the latest tag, now let's write a CD job that deploys the chart into our cluster. First we need to setup our `kubectl` config, for making things easier i'll be using the [jshimko/kube-tools-aws](https://hub.docker.com/r/jshimko/kube-tools-aws) docker image, it's a lightweight image that has aws-cli, helm and kubectl preinstalled.

```
deploy:
  image: jshimko/kube-tools-aws
  stage: deploy
  before_script:
    - aws eks --region us-west-2 update-kubeconfig --name granchain-test // This will configure kubectl for us

  script:
    - helm upgrade --install nginx-chart ./nginx-chart

  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'

```

But we need to configure port forwarding, let's create a `k8s/cluster.yaml` file so we can configure the service:

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: grainchain-chart
  annotations:
spec:
  ingressClassName: nginx
  rules:
  - http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: nginx-chart-nginx-grainchain
            port:
              number: 80
```

and let's apply it into the cluster:

```
kubectl apply -f ./k8s/cluster.yaml
```

And that should be enough for us to start using our application through our `nginx-controller`. We can visit the application at [http://a02cbf0b85ece4dd0a60508a5a9ea28a-772032779.us-west-2.elb.amazonaws.com](http://a02cbf0b85ece4dd0a60508a5a9ea28a-772032779.us-west-2.elb.amazonaws.com).

And now our grafana dashboard shows some metrics:
![](images/metrics.png)