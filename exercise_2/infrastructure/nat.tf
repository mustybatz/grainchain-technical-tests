resource "aws_eip" "nat" {
  vpc = true

  depends_on = [
    aws_vpc.main,
  ]

  tags = {
    Name = "nat"
  }
}

resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.public-us-west-2a.id


  tags = {
    Name = "nat"
  }

  depends_on = [
    aws_internet_gateway.igw,
    aws_eip.nat,
    aws_subnet.public-us-west-2a,
    ]
}
